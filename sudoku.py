from pulp import *
R = range(1,10) #rows (1-9)
C = range(1,10) #cols (1-9) 
D = range(1,10) #digits (1-9)

prob = LpProblem("Sudoku",LpMinimize)
xrcd = LpVariable.dicts("x",(R,C,D),0,1,LpInteger)

#objective
prob += lpSum(xrcd[r][c][d] for r in R for c in C for d in D), ""

#constraint: one dig. per table entry
for r in R:
  for c in C:
    prob += lpSum(xrcd[r][c][d] for d in D) == 1, ""

#constraint: each dig. once per row
for d in D:
  for r in R:
    prob += lpSum(xrcd[r][c][d] for c in C) == 1, ""

#constraint: each dig. once per col
for d in D:
  for c in C:
    prob += lpSum(xrcd[r][c][d] for r in R) == 1, ""

#constraint: each dig. once per 3x3 subtable
for d in D:
  for i in range(3):
    for j in range(3):
      prob += lpSum(xrcd[r][c][d] for r in R if r>=3*i+1 and r<=3*i+3 for c in C if c>=3*j+1 and c<=3*j+3) == 1, ""

#given info
prob += xrcd[1][1][4] == 1, ""
prob += xrcd[1][6][5] == 1, ""
prob += xrcd[1][7][7] == 1, ""
prob += xrcd[2][6][9] == 1, ""
prob += xrcd[2][7][1] == 1, ""
prob += xrcd[3][1][6] == 1, ""
prob += xrcd[3][6][8] == 1, ""
prob += xrcd[3][7][3] == 1, ""

prob += xrcd[4][2][9] == 1, ""
prob += xrcd[4][5][6] == 1, ""
prob += xrcd[4][8][2] == 1, ""
prob += xrcd[5][2][5] == 1, ""
prob += xrcd[5][5][3] == 1, ""
prob += xrcd[5][8][8] == 1, ""
prob += xrcd[6][2][3] == 1, ""
prob += xrcd[6][5][4] == 1, ""
prob += xrcd[6][8][6] == 1, ""

prob += xrcd[7][3][3] == 1, ""
prob += xrcd[7][4][7] == 1, ""
prob += xrcd[7][9][9] == 1, ""
prob += xrcd[8][3][2] == 1, ""
prob += xrcd[8][4][1] == 1, ""
prob += xrcd[9][3][8] == 1, ""
prob += xrcd[9][4][2] == 1, ""
prob += xrcd[9][9][5] == 1, ""

prob.writeLP("sudoku.lp")
prob.solve() #GLPK())
print "Status:", LpStatus[prob.status]
print "Optimal cost:", value(prob.objective)

table = [[],[],[],[],[],[],[],[],[]]
for r in R:
  for c in C:
    for d in D:
      if value(xrcd[r][c][d])==1:
        table[r-1].append(d)
for r in R:
  print table[r-1]

