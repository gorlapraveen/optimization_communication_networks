#solving small program for ILP
from pulp import*
prob=LpProblem('small ILP problem from HB6',LpMinimize)
x1=LpVariable('x1',0,None,LpContinuous)
x2=LpVariable('x2',0,None,LpContinuous)
#Objective declaration
prob +=3*x1+x2
#Defining Constaraints
prob +=x1+2*x2>=2
prob +=2*x1+x2>=2
prob.writeLP('small_problem.lp')
prob.solve(GLPK())
print 'Status:',LpStatus[prob.status]
print 'Optimal cost is :',value(prob.objective)
print 'optimal solution x1:',value(x1)
print 'x2:',value(x2)
