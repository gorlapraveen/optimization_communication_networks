#Python_code_for_Maximum_lifetiem_routing
#Python program for maximum lifetime routing in WSNs
#November 2017
from pulp import *
#default case: central node 0 located at sensor node 1
P = {0:(20,60),2:(40,40),3:(40,100),4:(60,20),5:(60,80),6:(80,40),\
     7:(80,100),8:(100,80)} #node positions (m)
R = 60 #coverage radius (m)
N = P.keys() #set of all nodes (node 0 being central node)
Np = [i for i in P.keys() if i!= 0] #N'= set of sensor nodes with traffic
D = {} #dictionary whose keys are links and whose values are distances (m)
A = {} #dictionary whose keys are nodes and whose values are adjacent nodes
for i in N:
  A[i] = []
  for j in [j for j in N if j!=i]:
    tmp = pow(P[i][0]-P[j][0],2)+pow(P[i][1]-P[j][1],2)
    if tmp<=pow(R,2):
      D[(i,j)] = tmp
      A[i].append(j)
L = D.keys(); #set of links
t = dict([(i,0.1) for i in Np]) #traffic from sensor nodes (packet/s)
PE_elec = 50e-6 #energy for transmitting/receiving 1 bit (J/bit)
PE_amp = 100e-9 #energy dissipation from transmit amplifier (J/bit/m^2)
gamma = 2 #path loss exponent
alpha_t = {}
for l in L:
  alpha_t[l] = PE_elec + PE_amp*D[l]
alpha_r = PE_elec
E = 1000.0 #initial battery energy (J)

################################################
prob = LpProblem('MLR_WSN',LpMinimize)
f = LpVariable.dicts('f',(Np,L),0,None,LpContinuous)
Linv = LpVariable('Linv',0,None,LpContinuous)

#objective
prob += Linv
#prob += Linv >= 0
#constraint: definition of Linv
#...TO BE COMPLETED BY YOU...
for i in Np:
 prob+=Linv>=lpSum(alpha_t[l]*f[k][l] for k in Np for l in L if l[0]==i)+lpSum(alpha_r*f[k][l] for k in Np for l in L if l[1]==i)

#constraint: flow conservation
#...TO BE COMPLETED BY YOU...
for i in N :
 for k in Np:
  if i==k:
   prob+=lpSum(f[k][l] for l in L if l[1]==i)-lpSum(f[k][l] for l in L if l[0]==i)==-t[k]
  elif i==0:
   prob+=lpSum(f[k][l] for l in L if l[1]==i)-lpSum(f[k][l] for l in L if l[0]==i)==t[k]
  else:
   prob+=lpSum(f[k][l] for l in L if l[1]==i)-lpSum(f[k][l] for l in L if l[0]==i)==0
for k in Np:
 for l in L:
  prob+=f[k][l]>=0

prob.writeLP('MLR_WSN.lp')
prob.solve(GLPK())
print 'Status:', LpStatus[prob.status]
print 'Optimal cost:', value(prob.objective)
for i in prob.variables(): #print only nonzero variables
  if value(i)!=0:
    print i,value(i)
print 'Lifetime:', 1/value(prob.objective)/3600.0/24.0 #nework lifetime (day)

